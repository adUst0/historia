<?php
if (!isset($_GET['page'])) $page = "inicio";
else $page = $_GET['page'];

switch ($page){
	case "inicio":
	  $include_file = "inicio.html";
	  $header = "Inicio";
	  break;
	case "sobreBG":
	  $include_file = "aboutBG.html";
	  $header = "About";
	  break;	
	case "sobre":
	  $include_file = "aboutES.html";
	  $header = "Sobre el proyecto";
	  break;		  
	case "colonias":
	  $include_file = "colonias.html";
	  $header = "Colonias";
	  break;
	case "constitucion":
	  $include_file = "constitucion.html";
	  $header = "Constitución";
	  break;
	case "independencia":
	  $include_file = "independencia.html";
	  $header = "Independencia";
	  break;
	case "politica":
	  $include_file = "politica.html";
	  $header = "Política";
	  break;	
	case "juego":
	  $include_file = "juego/juego.html";
	  $header = "Juego";
	  break;	  
	default:
	  $include_file = "inicio.html";
	  $header = "Inicio";
}
?>
<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $header ?> </title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="background">
		<div id="header">
			<div>
			<?php
				include "content/nav.html";
			?>
			</div>
		</div>
		<div id="body">
			<?php 
				if($page=='inicio') include "content/div2.html"; 
				if($page=='juego') include "content/juego/juego.html";
				if($page!='inicio' && $page!='juego') include "content/div1.html"; 
			?>
		</div>
	</div>
</body>
</html>